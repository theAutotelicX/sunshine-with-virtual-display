# sunshine-with-virtual-display

Sunshine and Moonlight streaming
---

Normally sunshine don't support virtual display, so here how to


Download and Install this to get virtual display in your device. 
```
https://www.amyuni.com/forum/viewtopic.php?t=3030
```

You can view it in the system menu.

Alternative solution for those who want more than 60Hz

```
https://github.com/ge9/IddSampleDriver/releases
```

And follow their installation guide.

---

Next, after activate additional display, view the display output for using further with Sunshine

```
C:\Program Files\Sunshine\tools\dxgi-info.exe
```

or

Create batch file for it
```
cd "C:\Program Files\Sunshine\tools\"
timeout 1
.\dxgi-info.exe
timeout 1
pause
```

![plot](./img/dxgi-info.png)

You see there is a "\\.\DISPLAY31" in the screen which not the same as we see in the system (normally we will see it as Screen3)

Put it in Sunshine AUDIO/VIDEO tab - Output Name
![plot](./img/sunshine.png)

Now, click on moonlight application, should work:)